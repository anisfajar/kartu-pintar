<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'awal';
$route['404_override'] = 'pagenotfound';
$route['translate_uri_dashes'] = FALSE;

// data master
$route['dashboard'] = 'tadmin/home/beranda';