<?php

class M_pesan extends CI_Model
{
    public function getdatapesan(){
        $this->db->select('*');
        $this->db->from('tb_pesan');
        $this->db->order_by('id_','ASC');
        return $this->db->get()->result();
    }

    public function simpanpesan($data){
        $this->db->insert('tb_pesan', $data);
    }

    public function jumlahpesanmasuk(){
        $sql = $this->db->query("SELECT COUNT('*') as totpesanmasuk FROM tb_pesan");
        return $sql->row();
    }
}