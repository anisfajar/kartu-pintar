<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_kartu extends CI_Model
{
    public function getdata()
    {
        $this->db->select('*');
        $this->db->from('tb_kartu a');
        $this->db->join('tb_jeniskartu b', 'a.id_jeniskartu=b.id_jeniskartu');
        $this->db->order_by('id_kartu', 'ASC');
        return $this->db->get()->result();
    }

    public function jumlahkartu(){
        $sql = $this->db->query("SELECT COUNT('*') as totkartu FROM tb_kartu");
        return $sql->row();
    }

    public function getById($id)
    {
        $this->db->where('id_kartu',$id);
        return $this->db->get('tb_kartu')->row();
    }

    public function savekartu($data){
        $this->db->insert('tb_kartu', $data);
    }

    public function deletekartu($id)
    {
        $this->db->where('id_kartu', $id);
        return $this->db->delete('tb_kartu');
    }

    public function updatekartu($data,$id)
    {
        return $this->db->update('tb_kartu',$data,array('id_kartu' => $id));
    }

    public function kode_auto()   {
        $this->db->select('RIGHT(tb_kartu.id_kartu,4) as kode', FALSE);
        $this->db->order_by('id_kartu','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('tb_kartu');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
            $data = $query->row();      
            $kode = intval($data->kode) + 1;    
        } else {      
            $kode = 1;    
        }
        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
        $kodejadi = "KRT-".$kodemax;
        return $kodejadi;  
  }
}
