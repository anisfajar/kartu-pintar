<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model
{
    public function login($user,$pass)
    {
        $pass  = $this->hash($pass);

        $this->db->SELECT('*');
        $this->db->FROM('tb_user');
        $this->db->WHERE('id_user',$user);
        $this->db->WHERE('password',$pass);

        return $this->db->get();
    }

    public function getdata(){
        return $this->db->get('tb_user')->result();
    }

    public function getById($id)
    {
        $this->db->where('id_user',$id);
        return $this->db->get('tb_user')->row();
    }

    public function saveuser($data){
        $this->db->insert('tb_user', $data);
    }

    public function updateuser($data,$id)
    {
        return $this->db->update('tb_user',$data,array('id_user' => $id));
    }

    public function deleteuser($id)
    {
        $this->db->where('id_user', $id);
        return $this->db->delete('tb_user');
    }

    public function hash($string)
    {
        return hash('sha512', $string . config_item('encryption_key'));
    }
}