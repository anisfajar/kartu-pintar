<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_jeniskartu extends CI_Model
{
    public function getdata()
    {
        $this->db->order_by('id_jeniskartu', 'ASC');
        return $this->db->get('tb_jeniskartu')->result();
    }

    public function jumlahjeniskartu(){
        $sql = $this->db->query("SELECT COUNT('*') as totjeniskartu FROM tb_jeniskartu");
        return $sql->row();
    }

    public function getById($id)
    {
        $this->db->where('id_jeniskartu',$id);
        return $this->db->get('tb_jeniskartu')->row();
    }

    public function savejeniskartu($data){
        $this->db->insert('tb_jeniskartu', $data);
    }

    public function deletejeniskartu($id)
    {
        $this->db->where('id_jeniskartu', $id);
        return $this->db->delete('tb_jeniskartu');
    }

    public function updatejeniskartu($data,$id)
    {
        return $this->db->update('tb_jeniskartu',$data,array('id_jeniskartu' => $id));
    }

    public function kode_auto()   {
        $this->db->select('RIGHT(tb_jeniskartu.id_jeniskartu,4) as kode', FALSE);
        $this->db->order_by('id_jeniskartu','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('tb_jeniskartu');      //cek dulu apakah ada sudah ada kode di tabel.    
        if($query->num_rows() <> 0){      
            $data = $query->row();      
            $kode = intval($data->kode) + 1;    
        } else {      
            $kode = 1;    
        }
        $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
        $kodejadi = "JKN-".$kodemax;
        return $kodejadi;  
  }
}
