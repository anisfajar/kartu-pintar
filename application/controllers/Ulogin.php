<?php

class Ulogin extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_user');
    }   

    public function index(){
        $this->load->view('login');
    }

    public function auth_login(){
        $user       = trim($this->input->post('username'));
        $pass       = trim($this->input->post('password'));
        $log        = $this->m_user->login($user,$pass);
        
        $juml_row   = $log->row();
        $user       = $log->result_array();

         if ($juml_row > 0) {
            $newdata = $user[0];//semua data array 
            $this->session->set_userdata($newdata); // masukkan data dalam session
            redirect('dashboard');
         } else {
            $this->session->set_flashdata('errore','Username / Password Invalid ...');
            redirect('ulogin','refresh');
         }
    }

    public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('');
	}
}