<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_kartu');
		$this->load->model('m_jeniskartu');
	}

	public function index()
	{
		$data['chip'] = $this->m_kartu->getdata();
        $data['jenis'] = $this->m_jeniskartu->getdata();
	
		$this->load->view('landingpage', $data);
	}
}