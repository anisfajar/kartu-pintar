<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('m_kartu');
        $this->load->model('m_jeniskartu');
        $this->load->model('m_pesan');
    }

    public function beranda(){
        $data['totalkartu'] = $this->m_kartu->jumlahkartu();
        $data['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $data['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();
        $this->load->view('vadm/home',$data);
    }

    public function logout() 
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('');
	}
}