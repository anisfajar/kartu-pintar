<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jeniskartu extends CI_Controller{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_jeniskartu');
        $this->load->model('m_kartu');
        $this->load->model('m_pesan');
    }

    public function index()
    {
        $data['jenis']      = $this->m_jeniskartu->getdata();
        $data['kodeauto']   = $this->m_jeniskartu->kode_auto();
        $data['totalkartu'] = $this->m_kartu->jumlahkartu();
        $data['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $data['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();

        $this->load->view('vadm/jeniskartu/listjeniskartu',$data);
    }

    public function tambahjeniskartu()
    {
        $data['jenis']      = $this->m_jeniskartu->getdata();
        $data['kodeauto']   = $this->m_jeniskartu->kode_auto();
        $data['totalkartu'] = $this->m_kartu->jumlahkartu();
        $data['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $data['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();

        $this->load->view('vadm/jeniskartu/tambahjeniskartu',$data);
    }

    public function addjeniskartu()
    {
        $idjeniskartu   = $this->input->post('id_jeniskartu');
        $namajeniskartu = $this->input->post('nama_jeniskartu');

        $cek = $this->db->query("SELECT * FROM tb_jeniskartu WHERE id_jeniskartu = '$idjeniskartu' ")->num_rows();
        if ($cek<=0) {
            $data = array(
                'id_jeniskartu'     => $idjeniskartu,
                'nama_jeniskartu'   => $namajeniskartu
            );

            $this->m_jeniskartu->savejeniskartu($data);
            $this->session->set_flashdata('success', 'Data jenis kartu berhasil disimpan...');
            // $this->session->set_flashdata('msg', '<div class="alert alert-success"><h4>Berhasil</h4></div>');
            redirect('tadmin/jeniskartu');
        } else {
            $this->session->set_flashdata('errore','Maaf ID tersebut sudah ada...');
            redirect('tadmin/jeniskartu');
        }
    }

    public function edit($id)
    {
        $datalist       = $this->m_jeniskartu;
        $val['result']  = $datalist->getById($id);
        $val['totalkartu'] = $this->m_kartu->jumlahkartu();
        $val['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $val['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();
        
        $this->load->view("vadm/jeniskartu/ubahjeniskartu", $val);
    }

    public function ex_edit($id = null)
    {
        $id = $this->input->post('id_jeniskartu');
        $data = array(
                'nama_jeniskartu' => $this->input->post('nama_jeniskartu')
            );
            
        $this->m_jeniskartu->updatejeniskartu($data,$id);
        $this->session->set_flashdata('success', 'Data jenis kartu berhasil diubah...');
        redirect('tadmin/jeniskartu');            
    }

    public function ex_del($id = null)
    {
        $this->m_jeniskartu->deletejeniskartu($id);
        $this->session->set_flashdata('notification','Hapus data sukses...');
        redirect('tadmin/jeniskartu');
    }
}