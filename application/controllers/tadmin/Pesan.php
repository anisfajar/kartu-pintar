<?php

class Pesan extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('m_pesan');
        $this->load->model('m_kartu');
        $this->load->model('m_jeniskartu');
    }

    public function index(){
        $data['pesan'] = $this->m_pesan->getdatapesan();
        $data['totalkartu'] = $this->m_kartu->jumlahkartu();
        $data['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $data['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();
        $this->load->view('vadm/pesan/listpesan',$data);
    }

    public function savepesan(){
        $id_        = $this->input->post('id_');
        $fname      = $this->input->post('fname');
        $lname      = $this->input->post('lname');
        $email      = $this->input->post('email');
        $phone      = $this->input->post('phone');
        $isi_pesan  = $this->input->post('isi_pesan');

        $data = array(
            'id_'        => $id_,
            'fname'      => $fname,
            'lname'      => $lname,
            'email'      => $email,
            'phone'      => $phone,
            'isi_pesan'  => $isi_pesan
        );
        $this->m_pesan->simpanpesan($data);
        $this->session->set_flashdata('success','Pesan berhasil dikirim...');
        redirect(site_url('message'));
    }
}