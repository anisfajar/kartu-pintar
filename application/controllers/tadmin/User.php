<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_kartu');
        $this->load->model('m_jeniskartu');
        $this->load->model('m_pesan');
    }

    public function index(){
        $data['user']      = $this->m_user->getdata();
        $data['totalkartu'] = $this->m_kartu->jumlahkartu();
        $data['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $data['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();
        $this->load->view('vadm/user/listuser',$data);
    }

    public function tambahuserpengguna()
    {
        $data['totalkartu'] = $this->m_kartu->jumlahkartu();
        $data['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $data['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();
        $this->load->view('vadm/user/tambahuser',$data);
    }

    public function adduser(){
        $id_user  = $this->input->post('id_user');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $hashpass = hash('sha512', $password . config_item('encryption_key'));

        $cek = $this->db->query("SELECT * FROM tb_user WHERE id_user = '$id_user' ")->num_rows();
        if ($cek<=0) {
            $data = array(
                'id_user'      => $id_user,
                'username'     => $username,
                'password'     => $hashpass
            );

            $this->m_user->saveuser($data);
            $this->session->set_flashdata('success','Penyimpanan data sukses...');
            redirect('tadmin/user');
        } else {
            $this->session->set_flashdata('errore','Maaf ID tersebut sudah ada...');
            redirect('tadmin/user');
        }
    }

    public function edit($id){
        $val['result'] = $this->m_user->getById($id);
        $val['totalkartu'] = $this->m_kartu->jumlahkartu();
        $val['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $val['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();
        $this->load->view("vadm/user/ubahuser", $val);
    }

    public function ex_edit($id = null)
    {
        $id_user   = $this->input->post('id_user');
        $username  = $this->input->post('username');
        $password  = $this->input->post('password');
        $hashpass  = hash('sha512', $password . config_item('encryption_key'));
        
        $data = array(
                'username' => $username,
                'password' => $hashpass
            );
            
        $this->m_user->updateuser($data,$id_user);
        $this->session->set_flashdata('success','Ubah data sukses...');
        redirect('tadmin/user');            
    }

    public function ex_del($id = null)
    {
        $this->m_user->deleteuser($id);
        $this->session->set_flashdata('notification','Hapus data sukses...');
        redirect('tadmin/user');
    }
}