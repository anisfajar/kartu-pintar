<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kartu extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->model('m_kartu');
        $this->load->model('m_jeniskartu');
        $this->load->model('m_pesan');
    }

    public function index(){
        $data['kartu']      = $this->m_kartu->getdata();
        $data['jeniskartu'] = $this->m_jeniskartu->getdata();
        $data['kodeauto']   = $this->m_kartu->kode_auto();
        $data['totalkartu'] = $this->m_kartu->jumlahkartu();
        $data['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $data['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();

        $this->load->view('vadm/kartu/listkartu',$data);
    }

    public function tambahkartu()
    {
        $data['jeniskartu'] = $this->m_jeniskartu->getdata();
        $data['kodeauto']   = $this->m_kartu->kode_auto();
        $data['totalkartu'] = $this->m_kartu->jumlahkartu();
        $data['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $data['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();

        $this->load->view('vadm/kartu/tambahkartu',$data);
    }

    public function addkartu(){

        $id_kartu       = $this->input->post('id_kartu');
        $nama_kartu     = $this->input->post('nama_kartu');
        $storage_kartu  = $this->input->post('storage_kartu');
        $ketkartu       = $this->input->post('keterangan_kartu');
        $id_jeniskartu  = $this->input->post('id_jeniskartu');
        
        //start upload file foto
        $namafilefoto              = "FOTO".$id_kartu;
        $configft['upload_path']   = './upload/foto/';
        $configft['allowed_types'] = "png|jpg|jpeg|gif|rar|zip";
        $configft['max_size']      = 1024;
        $configft['file_name']     = $namafilefoto;
        $configft['remove_spaces'] = TRUE;
        $configft['overwrite']     = TRUE;
        
        $this->load->library('upload');
        $this->upload->initialize($configft);

        $this->upload->do_upload('foto_kartu');
        $dataimg    = $this->upload->data('file_name');
        $locurl     = base_url().'upload/foto/';
        $pict       = $locurl.$dataimg;
        //end upload file foto

        $cek = $this->db->query("SELECT * FROM tb_kartu WHERE id_kartu = '$id_kartu' ")->num_rows();
        if ($cek<=0) {
            $data = array(
                'id_kartu'          => $id_kartu,
                'nama_kartu'        => $nama_kartu,
                'storage_kartu'     => $storage_kartu,
                'keterangan_kartu'  => $ketkartu,
                'id_jeniskartu'     => $id_jeniskartu,
                'file_foto'         => $pict
            );

            $this->m_kartu->savekartu($data);
            $this->session->set_flashdata('success','Penyimpanan data sukses...');
            redirect('tadmin/kartu');
        } else {
            $this->session->set_flashdata('errore','Maaf ID tersebut sudah ada...');
            redirect('tadmin/kartu');
        }
    }

    public function edit($id){

        $datalist           = $this->m_kartu;
        $val['result']      = $datalist->getById($id);
        $val['jeniskartu']  = $this->m_jeniskartu->getdata();
        $val['totalkartu'] = $this->m_kartu->jumlahkartu();
        $val['totaljeniskartu'] = $this->m_jeniskartu->jumlahjeniskartu();
        $val['totalpesan'] = $this->m_pesan->jumlahpesanmasuk();
        
        $this->load->view("vadm/kartu/ubahkartu", $val);
    }

    public function ex_edit($id = null)
    {
        $id             = $this->input->post('id_kartu');
        $nama_kartu     = $this->input->post('nama_kartu');
        $storage_kartu  = $this->input->post('storage_kartu');
        $id_jeniskartu  = $this->input->post('id_jeniskartu');
        $ketkartu       = $this->input->post('keterangan_kartu');

        $data = array(
                'nama_kartu'        => $nama_kartu,
                'storage_kartu'     => $storage_kartu,
                'keterangan_kartu'  => $ketkartu,
                'id_jeniskartu'     => $id_jeniskartu
            );
            
        $this->m_kartu->updatekartu($data,$id);
        $this->session->set_flashdata('success','Ubah data sukses...');
        redirect('tadmin/kartu');            
    }

    public function ex_del($id = null)
    {
        $this->m_kartu->deletekartu($id);
        $this->session->set_flashdata('notification','Hapus data sukses...');
        redirect('tadmin/kartu');
    }

 
}