<?php

class Katalog extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('m_kartu');
    }

    public function index(){
        $data['katalog'] = $this->m_kartu->getdata();
        $this->load->view('katalogkartu',$data);
    }
}