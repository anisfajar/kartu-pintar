<?php 
    $this->output->set_header('Last-Modified:' . gmdate('D, d M Y H:i:s') . 'GMT');
    $this->output->set_header('Cache-Control: no-cache, no-cache, must-revalidate');
    $this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
    $this->output->set_header('Pragma: no-cache'); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" href="<?php echo site_url('vasset/img/ikon.png') ?>" type="png">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404 Page Not Found</title>
</head>
<body style="background-image:url('../vasset/img/back404.jpg')">
<center>
    <p><br><p><br><p>
    <img src="<?php echo site_url('vasset/img/logo.png') ?>">
    <h1>404 Page Not Found</h1> 
    <h3>Maaf, Halaman tidak ditemukan</h3>
    <h4>Copyright &copy; 2019 SEKRIPKU.WEB.ID</h4>
</center>    
</body>
</html>