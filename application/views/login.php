<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
    <head>
        <?php $this->load->view('vpartadm/meta.php'); ?>
    </head>
    <body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
        <section class="flexbox-container">
            <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 m-0">
                    <div class="card-header no-border">
                        <div class="card-title text-xs-center">
                            <div class="p-1"><img src="<?php echo site_url('vasset/img/logo-header.png') ?>" alt="branding logo"></div>
                        </div>
                        <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Masukkan Username dan Password</span></h6>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <form class="form-horizontal form-simple" method="POST" action="<?php echo site_url('ulogin/auth_login') ?>" novalidate>
                                <fieldset class="form-group position-relative has-icon-left mb-0">
                                    <input type="text" autocomplete="off" name="username" class="form-control form-control-lg input-lg" id="username" placeholder="Your Username" required>
                                    <div class="form-control-position">
                                        <i class="icon-head"></i>
                                    </div>
                                </fieldset>
                                <br>
                                <fieldset class="form-group position-relative has-icon-left">
                                    <input type="password" autocomplete="off" name="password" class="form-control form-control-lg input-lg" id="password" placeholder="Enter Password" required>
                                    <div class="form-control-position">
                                        <i class="icon-key3"></i>
                                    </div>
                                </fieldset>
                                <fieldset class="form-group row">
                                    <div class="col-md-6 col-xs-12 text-xs-center text-md-left">
                                       
                                    </div>
                                </fieldset>
                                <button type="submit" class="btn btn-info btn-lg btn-block"><i class="icon-unlock2"></i> Login</button>
                            </form>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="">
                            <!-- alert -->
                          <?php
                            if ($this->session->flashdata('errore')) {
                            ?>
                                <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  <strong><?php echo $this->session->flashdata('errore'); ?></strong>
                                </div>
                            <?php
                            }
                          ?>
                          <!-- /alert -->
                        </div>
                    </div>
                </div>
            </div>
         </section>
        </div>
      </div>
    </div>
    <?php $this->load->view('vpartadm/js.php'); ?>
    </body>
</html>