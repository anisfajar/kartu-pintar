<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
  <?php $this->load->view('vpartadm/meta.php'); ?>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
  <?php $this->load->view('vpartadm/navbar.php'); ?>
  <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <div class="main-menu-header">

    </div>
    <div class="main-menu-content">
      <?php $this->load->view('vpartadm/menu.php'); ?>
    </div>
  </div>
  <!-- / main menu-->
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <?php $this->load->view('vadm/stats.php'); ?>
        <div class="row match-height">
          <div class="col-xl-4 col-md-6 col-sm-12">
            <div class="" style="height: 440px;">
                  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('vpartadm/footer.php'); ?>
  <?php $this->load->view('vpartadm/js.php'); ?>
</body>
</html>