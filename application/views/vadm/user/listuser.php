<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
  <?php $this->load->view('vpartadm/meta.php'); ?>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
  <?php $this->load->view('vpartadm/navbar.php'); ?>
  <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <div class="main-menu-header">

    </div>
    <div class="main-menu-content">
      <?php $this->load->view('vpartadm/menu.php'); ?>
    </div>
  </div>
  <!-- / main menu-->
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <?php $this->load->view('vadm/stats.php'); ?>
        <div class="row match-height">
          <div class="col-xl-12 col-md-12 col-sm-12">
            <div class="" style="height: 440px;">
                <div class="content-body">
                    <a href="<?php echo site_url('tadmin/user/tambahuserpengguna') ?>"><button type="button" class="btn btn-info btn-min-width mr-1 mb-1">Tambah data</button></a> 
                    <section id="description" class="card">
                        <div class="card-header">
                        <h4 class="card-title">Data User Pengguna</h4>
                        </div>
                        <div class="card-body collapse in">
                        <div class="card-block">
                          <!-- alert -->
                          <?php
                            if ($this->session->flashdata('success')) {
                            ?>
                                <div class="alert alert-success alert-dismissible fade in mb-2" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                  <strong><?php echo $this->session->flashdata('success'); ?></strong>
                                </div>
                            <?php
                            }
                          ?>
                          <!-- /alert -->
                            <div class="card-text">
                            <div class="table-responsive">
                            <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>ID User</th>
                                        <th>Username</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 1;
                                        foreach ($user as $pengguna) {  
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $pengguna->id_user ?></td>
                                        <td><?php echo $pengguna->username ?></td>
                                        <td width="160px">
                                            <a href="<?php echo site_url('tadmin/user/edit/'.$pengguna->id_user) ?>" class="btn btn-outline btn-sm btn-info"><i class="fa fa-edit"></i> Edit</a>
                                            <a href="<?php echo site_url('tadmin/user/ex_del/'.$pengguna->id_user) ?>" class="btn btn-outline btn-sm btn-danger delete-link"><i class="fa fa-trash-o"></i> Hapus</a>
                                        </td>
                                    </tr>
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                            <?php
                                if ($this->session->flashdata('notification')) {
                                ?>
                                    <div class="alert alert-danger alert-dismissible fade in mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong><?php echo $this->session->flashdata('notification'); ?></strong>
                                    </div>
                                <?php
                                }
                            ?>
                            </div>
                            </div>
                        </div>
                        </div>
                    </section>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('vpartadm/footer.php'); ?>
  <?php $this->load->view('vpartadm/js.php'); ?>
    <!-- <script>
    $(document).ready(function() {
        $('#mydata').DataTable();
    } )
    </script> -->
</body>
</html>