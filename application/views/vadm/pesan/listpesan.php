<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
  <?php $this->load->view('vpartadm/meta.php'); ?>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
  <?php $this->load->view('vpartadm/navbar.php'); ?>
  <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <div class="main-menu-header">

    </div>
    <div class="main-menu-content">
      <?php $this->load->view('vpartadm/menu.php'); ?>
    </div>
  </div>
  <!-- / main menu-->
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <?php $this->load->view('vadm/stats.php'); ?>
        <div class="row match-height">
          <div class="col-xl-12 col-md-12 col-sm-12">
            <div class="" style="height: 440px;">
                <div class="content-body">
                    <section id="description" class="card">
                        <div class="card-header">
                        <h4 class="card-title">Pesan Masuk</h4>
                        </div>
                        <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="card-text">
                            <div class="table-responsive">
                            <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped" id="datapesan" width="100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>ID Pesan</th>
                                        <th>Nama Customer</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Isi Pesan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $no = 1;
                                        foreach ($pesan as $isipesan) {  
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $isipesan->id_ ?></td>
                                        <td><?php echo $isipesan->fname." ".$isipesan->lname ?></td>
                                        <td><?php echo $isipesan->email ?></td>
                                        <td><?php echo $isipesan->phone ?></td>
                                        <td><?php echo $isipesan->isi_pesan ?></td>
                                    </tr>
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                            </div>
                            </div>
                        </div>
                        </div>
                    </section>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('vpartadm/footer.php'); ?>
  <?php $this->load->view('vpartadm/js.php'); ?>
    <!-- <script>
    $(document).ready(function() {
        $('#mydata').DataTable();
    } )
    </script> -->
</body>
</html>