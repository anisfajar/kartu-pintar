<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
  <?php $this->load->view('vpartadm/meta.php'); ?>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
  <?php $this->load->view('vpartadm/navbar.php'); ?>
  <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <div class="main-menu-header">

    </div>
    <div class="main-menu-content">
      <?php $this->load->view('vpartadm/menu.php'); ?>
    </div>
  </div>
  <!-- / main menu-->
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <?php $this->load->view('vadm/stats.php'); ?>
        <div class="row match-height">
          <div class="col-xl-12 col-md-12 col-sm-12">
            <div class="" style="height: 440px;">
                <div class="content-body">
                    <section id="description" class="card">
                        <div class="card-header">
                        <h4 class="card-title">Ubah Data Kartu</h4>
                        </div>
                        <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="card-text">
                            <form class="form" action="<?php echo site_url('tadmin/kartu/ex_edit') ?>" method="post" enctype="multipart/form-data" >
                                <h4 class="form-section"><i class="icon-clipboard4"></i> Kartu Pintar / Smartcard</h4>
                                <div class="form-group">
                                    <label for="name">ID Kartu</label>
                                    <input class="form-control"
                                    type="text" name="id_kartu" readonly="" value="<?php echo $result->id_kartu ?>"  autocomplete="off" required="" placeholder="ID Jenis Kartu" />
                                </div>
                                <div class="form-group">
                                    <label for="name">Nama Kartu</label>
                                    <input class="form-control"
                                    type="text" name="nama_kartu" value="<?php echo $result->nama_kartu ?>"  autocomplete="off" required="" placeholder="Nama Jenis Kartu" />
                                </div>
                                <div class="form-group">
                                    <label for="name">Storage Kartu</label>
                                    <input class="form-control"
                                    type="text" name="storage_kartu" value="<?php echo $result->storage_kartu ?>"  autocomplete="off" required="" placeholder="Nama Jenis Kartu" />
                                </div>
                                <div class="form-group">
                                    <label for="name">Jenis Kartu</label>
                                    <select name="id_jeniskartu" id="idjenis" class="form-control" required="">
                                        <?php foreach($jeniskartu as $jenis) { ?>
                                            <option value="<?php echo $jenis->id_jeniskartu;?>"<?=($result->id_jeniskartu == $jenis->id_jeniskartu ? 'selected' : '')?>><?php echo $jenis->nama_jeniskartu;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name">Keterangan Kartu</label>
                                    <input class="form-control"
                                    type="text" name="keterangan_kartu" value="<?php echo $result->keterangan_kartu ?>"  autocomplete="off" required="" />
                                </div>

                                <div class="form-actions">
                                    <input type="submit" value="save" class="btn btn-success"></input>
                                    <a href="<?php echo site_url('tadmin/kartu') ?>"><button type="button" class="btn btn-warning mr-1">cancel</button></a> 
                                </div>
                            </form>
                            </div>
                        </div>
                        </div>
                    </section>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('vpartadm/footer.php'); ?>
  <?php $this->load->view('vpartadm/js.php'); ?>
    <!-- <script>
    $(document).ready(function() {
        $('#mydata').DataTable();
    } )
    </script> -->
</body>
</html>