<!-- stats row -->
<div class="row">
    <div class="col-xl-3 col-lg-6 col-xs-12">
    <div class="card">
        <div class="card-body">
        <div class="card-block">
            <div class="media">
            <div class="media-body text-xs-left">
                <h3 class="pink"><?php echo $totalkartu->totkartu ?></h3>
                <span>Total Kartu</span>
            </div>
            <div class="media-right media-middle">
                <i class="icon-credit-card pink font-large-2 float-xs-right"></i>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-xs-12">
    <div class="card">
        <div class="card-body">
        <div class="card-block">
            <div class="media">
            <div class="media-body text-xs-left">
                <h3 class="teal"><?php echo $totaljeniskartu->totjeniskartu ?></h3>
                <span>Total Jenis Kartu</span>
            </div>
            <div class="media-right media-middle">
                <i class="icon-th-list teal font-large-2 float-xs-right"></i>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-xs-12">
    <div class="card">
        <div class="card-body">
        <div class="card-block">
            <div class="media">
            <div class="media-body text-xs-left">
                <h3 class="deep-orange"><?php echo $totalpesan->totpesanmasuk ?></h3>
                <span>Total Pesan Masuk</span>
            </div>
            <div class="media-right media-middle">
                <i class="icon-inbox deep-orange font-large-2 float-xs-right"></i>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>
<!--/ stats row -->