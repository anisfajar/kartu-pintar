<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
  <?php $this->load->view('vpartadm/meta.php'); ?>
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">
  <?php $this->load->view('vpartadm/navbar.php'); ?>
  <div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <div class="main-menu-header">

    </div>
    <div class="main-menu-content">
      <?php $this->load->view('vpartadm/menu.php'); ?>
    </div>
  </div>
  <!-- / main menu-->
  <div class="app-content content container-fluid">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <?php $this->load->view('vadm/stats.php'); ?>
        <div class="row match-height">
          <div class="col-xl-12 col-md-12 col-sm-12">
            <div class="" style="height: 440px;">
                <div class="content-body">
                    <section id="description" class="card">
                        <div class="card-header">
                        <h4 class="card-title">Tambah Data Jenis Kartu</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <!-- <li><a data-action="expand"><i class="icon-expand2"></i></a></li> -->
                            </ul>
                        </div>
                        </div>
                        <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="card-text">
                            <form class="form" action="<?php echo site_url('tadmin/jeniskartu/addjeniskartu') ?>" method="post" enctype="multipart/form-data" >
                                <h4 class="form-section"><i class="icon-clipboard4"></i> Jenis Kartu Pintar / Smartcard</h4>
                                <div class="form-group">
                                    <label for="companyName">ID Jenis Kartu</label>
                                    <input type="text" readonly="" autocomplete="off" class="form-control" value="<?php echo $kodeauto; ?>" name="id_jeniskartu">
                                </div>
                                <div class="form-group">
                                    <label for="companyName">Nama Jenis Kartu</label>
                                    <input type="text" autocomplete="off" class="form-control" value="" name="nama_jeniskartu">
                                </div>

                                <div class="form-actions">
                                    <input type="submit" value="+ Save" class="btn btn-success"></input>
                                    <a href="<?php echo site_url('tadmin/jeniskartu') ?>"><button type="button" class="btn btn-warning mr-1">x Cancel</button></a> 
                                </div>
                            </form>
                            </div>
                        </div>
                        </div>
                    </section>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('vpartadm/footer.php'); ?>
  <?php $this->load->view('vpartadm/js.php'); ?>
    <!-- <script>
    $(document).ready(function() {
        $('#mydata').DataTable();
    } )
    </script> -->
</body>
</html>