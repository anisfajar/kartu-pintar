<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view('vpartawal/head'); ?>
</head>
<body>
    <?php $this->load->view('vpartawal/header.php'); ?>
    <!-- row -->
    <div class="row">
        <div class="home-posts">
        <div class="span12">
            <h3>Katalog Kartu pintar</h3>
        </div>
        <?php
            foreach ($katalog as $row) {
        ?>
        <div class="span4">
            <div class="post-image">
            <a href="">
                <img width="300px" src="<?php echo base_url(); ?>assets/img/smart.png" alt="">
            </a>
            </div>
            <div class="entry-meta">
            <a href="#"><i class="icon-square icon-48 icon-list left"></i></a>
            </div>
            <!-- end .entry-meta -->
            <div class="entry-body">
            <a href="">
                <h5 class="title"><?php echo $row->nama_kartu ?></h5>
            </a>
            <p>
                <table class="table">
                <tr>
                    <td>Nama</td>
                    <td><?php echo $row->nama_kartu ?></td>
                </tr>
                <tr>
                    <td>Jenis</td>
                    <td><?php echo $row->nama_jeniskartu ?></td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td><?php echo $row->keterangan_kartu ?></td>
                </tr>
                </table>
            </p>
            </div>
            <!-- end .entry-body -->
            <div class="clear">
            </div>
        </div>
        <?php } ?>

        </div>
    </div>
    <!-- /row -->
    </div>
  </section>

  <?php $this->load->view('vpartawal/footer.php'); ?>
  <?php $this->load->view('vpartawal/js.php'); ?>
</body>
</html>