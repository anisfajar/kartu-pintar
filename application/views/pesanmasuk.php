<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('vpartawal/head'); ?>
</head>
<body>
    <?php $this->load->view('vpartawal/header.php'); ?>
      <div class="row">
      <form action="<?php echo site_url('tadmin/pesan/savepesan') ?>" method="POST" class="contactForm">
        <div class="span4">
          <aside>
            <div class="widget">
              <h4>Kontak Kami</h4>
              <ul>
                <li><label><strong>Phone : </strong></label>
                  <p>
                    +62 857 271 092 86
                  </p>
                </li>
                <li><label><strong>Email : </strong></label>
                  <p>
                    f_dev@sekripku.web.id
                  </p>
                </li>
                <li><label><strong>Adress : </strong></label>
                  <p>
                    Kudus, Indonesia
                  </p>
                </li>
              </ul>
            </div>
          </aside>
        </div>
        <div class="span8">
          <div id=""><h4>Silahkan kirimkan pesan anda kepada kami</h4></div>
            <div class="row">
              <div class="span8 form-group">
                <input type="hidden" name="id_" autocomplete="off" readonly="" required="" value="ID<?php echo date('YmdHis')?>" class="form-control" placeholder="ID" />
              </div>
              <div class="span4 form-group">
                <input type="text" name="fname" autocomplete="off" required="" class="form-control" placeholder="Nama Depan" />
              </div>
              <div class="span4 form-group">
                <input type="text" name="lname" class="form-control" autocomplete="off" required="" placeholder="Nama Belakang" />
              </div>
              <div class="span8 form-group">
                <input type="email" name="email" class="form-control" autocomplete="off" required="" placeholder="Masukkan email anda dengan benar" />
              </div>
              <div class="span8 form-group">
                <input type="text" name="phone" class="form-control" autocomplete="off" required="" placeholder="No. Hp" />
              </div>
              <div class="span8 form-group">
                <textarea required="" class="form-control" name="isi_pesan" rows="5"
                  placeholder="Silahkan masukkan pesan anda"></textarea>
              <button type="submit" name="simpan" value="Kirim" id="submit" class="btn btn-info btn-radius btn-brd grd1">Kirim pesan </button>
              </div>
            </div>
            
        </div>
      </form>
      </div>
  </div>
  </section>
  <?php $this->load->view('vpartawal/footer.php'); ?>
  <?php $this->load->view('vpartawal/js.php'); ?>
</body>
</html>