<link rel="icon" href="<?php echo site_url('vasset/img/ikon.png') ?>" type="png">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
<meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
<meta name="author" content="PIXINVENT">
<title>SI - Kartu Pintar</title>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/css/bootstrap.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/fonts/icomoon.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/fonts/flag-icon-css/css/flag-icon.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/vendors/css/extensions/pace.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/css/bootstrap-extended.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/css/app.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/css/colors.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/css/core/menu/menu-types/vertical-menu.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/css/core/colors/palette-gradient.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/app-assets/css/pages/login-register.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('vasset/assets/css/style.css') ?>">