<ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
    <li class=" nav-item">
        <a href="<?php echo site_url('dashboard') ?>"><i class="icon-home3"></i>
            <span data-i18n="nav.dash.main" class="menu-title">Dashboard</span>
        </a>
    </li>
    
    <li class=" nav-item"><a href="#"><i class="icon-briefcase4"></i><span data-i18n="nav.project.main"
            class="menu-title">Data Master</span></a>
        <ul class="menu-content">
            <li><a href="<?php echo site_url('tadmin/jeniskartu') ?>" data-i18n="nav.invoice.invoice_template" class="menu-item"> Data Jenis Kartu</a>
            </li>
            <li><a href="<?php echo site_url('tadmin/kartu') ?>" data-i18n="nav.gallery_pages.gallery_grid" class="menu-item">Data Kartu Pintar</a>
            </li>
            <li><a href="<?php echo site_url('tadmin/pesan') ?>" data-i18n="nav.search_pages.search_page" class="menu-item">Pesan Masuk</a>
            </li>
            <li><a href="<?php echo site_url('tadmin/user') ?>" data-i18n="nav.search_pages.search_website" class="menu-item">Data User Pengguna</a>
            </li>
        </ul>
    </li>

</ul>