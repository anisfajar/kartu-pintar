<meta charset="utf-8">
<title>Kartu Pintar</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- styles -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,700" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/docs.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/prettyPhoto.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/js/google-code-prettify/prettify.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/flexslider.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/sequence.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/color/default.css" rel="stylesheet">
<link rel="icon" href="<?php echo base_url(); ?>assets/img/sc.png" type="png">