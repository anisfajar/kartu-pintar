<header>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand logo" href="index.html"><img src="<?php echo base_url(); ?>assets/img/logo-header.png" alt=""></a>
          <div class="navigation">
            <nav>
              <ul class="nav topnav">
                <li class="dropdown">
                  <a href="<?php echo site_url('') ?>">Home</a>
                </li>
                <li class="dropdown">
                  <a href="<?php echo site_url('katalog') ?>">Katalog</a>
                </li>
                <li>
                  <a href="<?php echo site_url('message') ?>">Contact</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>
  <section id="intro">
    <div class="jumbotron masthead">
      <div class="container">
        <div class="sequence-nav">
          <div class="prev">
            <span></span>
          </div>
          <div class="next">
            <span></span>
          </div>
        </div>
        <div class="row">
          <div class="span12">
            <div id="slider_holder">
              <div id="sequence">
                <ul>
                  <li>
                    <div class="info animate-in">
                      <h2>Smartcard / Kartu Pintar</h2>
                      <br><p><br>
                        Sebuah kartu yang telah dipendam sirkuit terpadu. Meskipun banyak kegunaannnya, 
                        namun ada dua pembagian dasar dari kartu ini, yaitu kartu memori dan kartu dengan 
                        mikroprosesor.
                      </p>
                    </div>
                    <img class="slider_img animate-in" src="<?php echo base_url(); ?>assets/img/kartu.jpg" alt="">
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section id="maincontent">
    <div class="container">
    <?php
      if ($this->session->flashdata('success')) {
      ?>
          <div class="alert alert-success alert-dismissible fade in mb-2" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <strong><?php echo $this->session->flashdata('success'); ?></strong>
          </div>
      <?php
      }
    ?>
      <h3>Jenis - Jenis Kartu pintar</h3>
      <p>
        <div class="row">
          <div class="span4 features">
            <i class="icon-circled icon-32 icon-credit-card left active"></i>
            <h4>Java Card</h4>
            <div class="dotted_line">
            </div>
            <p class="left">
              Java Card adalah bagian dari platform Java yang ditujukan untuk smart card. Java menyediakan package
              javax.smartcardio untuk keperluan tersebut.
            </p>
          </div>
          <div class="span4 features">
            <i class="icon-circled icon-32 icon-credit-card left active"></i>
            <h4>Microprocessor Card</h4>
            <div class="dotted_line">
            </div>
            <p class="left">
              Smart card ini mempunyai memory circuit dan microprocessor dalam satu chip. Semua akses ke kartu akan
              melalui microprocessor. Datanya sendiri baru bisa diakses jika telah melewati semacam security logic.
            </p>
          </div>
          <div class="span4 features">
            <i class="icon-circled icon-32 icon-credit-card left active"></i>
            <h4>Contact Card</h4>
            <div class="dotted_line">
            </div>
            <p class="left">
              Kartu ini adalah smart card yang mempunyai contact chip. Kartu ini harus dimasukkan ke reader untuk
              melakukan transaksi / menyampaikan informasi dari kartu ke reader.
            </p>
          </div>

        </div>
       
        <div class="row">
          <div class="span4 features">
            <i class="icon-circled icon-32 icon-credit-card left active"></i>
            <h4>Contactless Card</h4>
            <div class="dotted_line">
            </div>
            <p class="left">
              Kartu ini adalah jenis smart card yang menggunakan frekuensi radio (RF) untuk bertukar informasi. Jadi
              kartu ini tidak perlu kontak fisik ke reader/terminal untuk bertukar informasi.
            </p>
          </div>
          <div class="span4 features">
            <i class="icon-circled icon-32 icon-credit-card left active"></i>
            <h4>Hybrid Card</h4>
            <div class="dotted_line">
            </div>
            <p class="left">
              Smart card yang menggunakan dua teknologi yang ada di contact card dan contactless card. Sehingga terdapat
              alat contact dan antena dalam satukartu. Kartunya sendiri ada yang menggunakan satu microprocesor dan ada
              juga yang menggunakan dua microprocessor
            </p>
          </div>
          <div class="span4 features">
            <i class="icon-circled icon-32 icon-credit-card left active"></i>
            <h4>Memory Card</h4>
            <div class="dotted_line">
            </div>
            <p class="left">
              Kartu ini hanya mengandung memory circuit yang dapat diakses melalui kontak dengan synchronous protocol
            </p>
          </div>
        </div>