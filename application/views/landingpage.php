<?php 
   date_default_timezone_set('Asia/Jakarta');
?>
<!DOCTYPE html>
<html lang="en">
<title>Kartu Pintar</title>
<link rel="shortcut icon" href="<?php echo base_url('vasset/landingpage/images/sc.png') ?>" type="image/x-icon" />
<link rel="stylesheet" href="<?php echo base_url('vasset/landingpage/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('vasset/landingpage/style.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('vasset/landingpage/css/colors.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('vasset/landingpage/css/versions.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('vasset/landingpage/css/responsive.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('vasset/landingpage/css/building.css') ?>">
</head>
<body class="building_version" data-spy="scroll" data-target=".header">
   <div id="preloader">
      <img class="preloader" src=" alt="">
   </div>
   <header class="header header_style_01">
      <nav class="megamenu navbar navbar-default" data-spy="affix">
         <div class="top-header"></div>
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                  aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href=""><img src="<?php echo base_url('vasset/landingpage/images/sc.png') ?>" alt="image">
                  <span>Kartu Pintar</span></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
               <ul class="nav navbar-nav navbar-right">
                  <li class="active"><a data-scroll href="#home">Home</a></li>
                  <li><a data-scroll href="#carapesan">Cara Pesan</a></li>
                  <li><a data-scroll href="#jenis">Jenis Smart Card</a></li>
                  <li><a data-scroll href="#katalog">Katalog Smart Card</a></li>
                  <li><a data-scroll href="#contact">Kontak Kami</a></li>
               </ul>
            </div>
         </div>
      </nav>
   </header>
   <div id="home" class="parallax first-section" data-stellar-background-ratio="0.4"
      style="background-image:url('vasset/landingpage/images/kartu.jpg')">
      <div class="container">
         <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-md-offset-2 text-center">
               <div class="big-tagline">
                  <h2><span class="yellow">Kartu Pintar</span></h2>
                  <a data-scroll href="#katalog" class="btn btn-light btn-radius btn-brd">Lihat Katalog Smartcard</a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div id="carapesan" class="section wb">
      <div class="container">
         <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
               <div class="message-box">
                  <h2>Cara Pemesanan</h2>
                  <h5>Cara pemesanan kartu pintar</h5>
               </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
               <div class="message-box">
                  <p class="lead">Silahkan isi data pemesanan dengan format : </p>
                  <ul>
                     1. Nama Lengkap : <br>
                     2. Nomor Identitas (KTP / SIM / Passport) : <br>
                     3. Alamat Lengkap : <br>
                     4. Nomor Telepon : <br>
                     5. Photo KTP : <br>
                     6. Katalog Kartu yang diinginkan : <br>
                  </ul>
                  <ul>
                     Kirimkan langsung via whatsapp ke nomor : 085-727-109-286</p>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="sep1"></div>
   </div>
   <div id="jenis" class="parallax section db parallax-off no-padding-bottom">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="message-box">
                  <h2>Jenis Smart Card</h2>
                  <h5>Jenis - Jenis Kartu Pintar</h5>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 serv" style="background-color:teal;">
               <div class="serv-blog">
                  <img src="<?php echo base_url('vasset/landingpage/images/sc.png')?>" alt="#" />
               </div>
               <div class="serv-blog-cont">
                  <h3>Java Card</h3>
                  <p>Java Card adalah bagian dari platform Java yang ditujukan untuk smart card. 
                  Java menyediakan package javax.smartcardio untuk keperluan tersebut.</p>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 serv" style="background:teal;">
               <div class="serv-blog">
                  <img src="<?php echo base_url('vasset/landingpage/images/sc.png')?>" alt="#" />
               </div>
               <div class="serv-blog-cont">
                  <h3>Microprocessor Card</h3>
                  <p>Smart card ini mempunyai memory circuit dan microprocessor dalam satu chip. 
                  Semua akses ke kartu akan melalui microprocessor. Datanya sendiri baru bisa diakses 
                  jika telah melewati semacam security logic.</p>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 serv" style="background:teal;">
               <div class="serv-blog">
                  <img src="<?php echo base_url('vasset/landingpage/images/sc.png')?>" alt="#" />                  
               </div>
               <div class="serv-blog-cont">
                  <h3>Contact Card</h3>
                  <p>Kartu ini adalah smart card yang mempunyai contact chip. Kartu ini harus dimasukkan 
                  ke reader untuk melakukan transaksi / menyampaikan informasi dari kartu ke reader.</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 serv" style="background-color:teal;">
               <div class="serv-blog">
                  <img src="<?php echo base_url('vasset/landingpage/images/sc.png')?>" alt="#" />
               </div>
               <div class="serv-blog-cont">
                  <h3>Contactless Card</h3>
                  <p>Kartu ini adalah jenis smart card yang menggunakan frekuensi radio (RF) 
                     untuk bertukar informasi. Jadi kartu ini tidak perlu kontak fisik ke reader/terminal 
                     untuk bertukar informasi.</p>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 serv" style="background:teal;">
               <div class="serv-blog">
                  <img src="<?php echo base_url('vasset/landingpage/images/sc.png')?>" alt="#" />
               </div>
               <div class="serv-blog-cont">
                  <h3>Hybrid Card</h3>
                  <p>Smart card yang menggunakan dua teknologi yang ada di contact card dan contactless card.
                     Sehingga terdapat alat contact dan antena dalam satukartu. Kartunya sendiri ada 
                     yang menggunakan satu microprocesor dan ada juga yang menggunakan dua microprocessor</p>
               </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 serv" style="background:teal;">
               <div class="serv-blog">
                  <img src="<?php echo base_url('vasset/landingpage/images/sc.png')?>" alt="#" />                  
               </div>
               <div class="serv-blog-cont">
                  <h3>Memory Card</h3>
                  <p>Kartu ini hanya mengandung memory circuit yang dapat diakses melalui kontak dengan synchronous protocol</p>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div id="katalog" class="parallax section db parallax-off no-padding-bottom">
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="message-box">
                  <h2>Katalog Smart Card</h2>
                  <h5>Katalog Kartu Pintar</h5>
               </div>
            </div>
         </div>
      </div>
      
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="row">
            <?php foreach ($chip as $kartu) {
               $foto       = $kartu->file_foto;
               $namakartu  = $kartu->nama_kartu;
               $ketkartu   = $kartu->keterangan_kartu;
               $storage    = $kartu->storage_kartu;
            ?>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 serv" style="background-color:seagreen;">
               <div class="serv-blog">
                  <img class="img-rounded" width="400px" height="125px" src="<?php echo $kartu->file_foto ?>" alt="#" />
               </div>
               <div class="serv-blog-cont">
                  <p>Nama Kartu : <?php echo $namakartu ?></p>
                  <p>Storage Kartu : <?php echo $storage; ?></p>
                  <p>Keterangan : <?php echo $ketkartu; ?></p>
               </div>
            </div>
            <?php } ?>
         </div>
      </div>
   </div>

   <div id="contact" class="section wb">
      <div class="container">
         <div class="section-title row text-center">
            <div class="col-md-8 col-md-offset-2">
               <h3>Kontak kami</h3>
               <p class="lead">Silahkan isi di bawah ini jika ingin mengontak kami lewat pesan di bawah ini</p>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-4 col-md-4">
               <div class="contant-info">
                  <ul class="item-display-block">
                     <li>
                        <div class="info-shape accent-color background fs-23">
                           <div class="icon"><i class="fa fa-home"></i></div>
                        </div>
                        <div class="info-content">
                           <h6 class="uppercase"> Alamat:</h6>
                           <p> Kudus, Jawa Tengah, Indonesia</p>
                        </div>
                     </li>
                     <li>
                        <div class="info-shape accent-color background fs-23">
                           <div class="icon"><i class="fa fa-volume-control-phone"></i></div>
                        </div>
                        <div class="info-content">
                           <h6 class="uppercase"> No. Telepon:</h6>
                           <p> 085-727-109-286<br>
                        </div>
                     </li>
                     <li>
                        <div class="info-shape accent-color background fs-23">
                           <div class="icon"><i class="fa fa-envelope-o"></i></div>
                        </div>
                        <div class="info-content">
                           <h6 class="uppercase"> Alamat Email:</h6>
                           <p>f_dev@sekripku.web.id</p>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="col-md-8">
               <div class="contact_form">
                  <div id="message"></div>
                  <form class="row" method="POST" action="<?php echo site_url('admin/pesan/savepesan') ?>">
                     <fieldset class="row-fluid">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <input type="hidden" required="" readonly="" value="ID<?php echo date('YmdHis')?>" name="id_" autocomplete="off" id="" class="form-control"
                              placeholder="ID">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                           <input type="text" required="" name="fname" autocomplete="off" id="" class="form-control"
                              placeholder="Nama Depan">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                           <input type="text" required="" name="lname" autocomplete="off" id="" class="form-control"
                              placeholder="Nama Belakang">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                           <input type="email" required="" name="email" autocomplete="off" id="" class="form-control" placeholder= Email">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                           <input type="text" minlength="12" maxlength="12" required="" name="phone" autocomplete="off" id="" class="form-control" placeholder="No. Telp">
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                           <textarea required="" class="form-control" autocomplete="off" name="isi_pesan" id="" rows="6"
                              placeholder="Pesan anda.."></textarea>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                           <button type="submit" value="SEND" id="submit"
                              class="btn btn-light btn-radius btn-brd grd1 btn-block">Kirim </button>
                        </div>
                     </fieldset>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <footer id="footer">
      <div id="footer-widgets" class="container style-1">
         <div class="row">
            <div class="col-md-4">
               <div class="widget widget_text">
                  <h2 class="widget-title"><span>Tentang Kami</span></h2>
                  <div class="textwidget">
                     <a class="navbar-brand" href=""><img src="<?php echo base_url('vasset/landingpage/images/sc.png') ?>" alt="image"><span>Kartu
                           Pintar</span></a>
                     <p> Sebuah kartu yang telah dipendam sirkuit terpadu. Meskipun banyak kegunaannnya, 
                           namun ada dua pembagian dasar dari kartu ini, yaitu kartu memori dan kartu dengan 
                           mikroprosesor.</p>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="widget widget_links">
                  <h2 class="widget-title"><span>LINKS WEBSITE</span></h2>
                  <ul class="wprt-links clearfix col2">
                     <li><a data-scroll="" href="#home">Home</a></li>
                     <li><a data-scroll="" href="#carapesan">Cara Pesan</a></li>
                     <li><a data-scroll="" href="#jenis">Jenis Kartu</a></li>
                     <li><a data-scroll="" href="#katalog">Katalog Kartu</a></li>
                     <li><a data-scroll="" href="#contact">Kontak Kami</a></li>
                  </ul>
               </div>
            </div>
            <div class="col-md-4">
               <div class="widget widget_information">
                  <h2 class="widget-title"><span>KONTAK KAMI</span></h2>
                  <ul>
                     <li class="address clearfix">
                        <span class="hl">Alamat: </span>
                        <span class="text">&nbsp; Kudus, Jawa Tengah, Indonesia</span>
                     </li>
                     <li class="phone clearfix">
                        <span class="hl">No.Telp.</span>
                        <span class="text">&nbsp; 085-727-109-286</span>
                     </li>
                     <li class="email clearfix">
                        <span class="hl">E-mail:</span>
                        <span class="text">info@sekripku.web.id</span>
                     </li>
                  </ul>
               </div>
               <div class="widget widget_socials">
                  <div class="socials">
                     <a target="" href="#"><i class="fa fa-twitter"></i></a>
                     <a target="" href="#"><i class="fa fa-facebook"></i></a>
                     <a target="" href="#"><i class="fa fa-google-plus"></i></a>
                     <a target="" href="#"><i class="fa fa-linkedin"></i></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div id="bottom" class="clearfix style-1">
         <div class="container">
            <div id="bottom-bar-inner" class="wprt-container">
               <div class="bottom-bar-inner-wrap">
                  <div class="bottom-bar-content">
                     <div id="copyright">All Rights Reserved. Sekripku © 2019</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </footer>
   <a href="#home" data-scroll class="dmtop global-radius"><i class="fa fa-hand-o-up"></i></a>
   <script src="<?php echo base_url('vasset/landingpage/js/all.js') ?>"></script>
   <script src="<?php echo base_url('vasset/landingpage/js/custom.js') ?>"></script>
   <script src="<?php echo base_url('vasset/landingpage/js/portfolio.js') ?>"></script>
   <script src="<?php echo base_url('vasset/landingpage/js/hoverdir.js') ?>"></script>
</body>
</html>