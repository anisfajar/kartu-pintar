-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 04 Okt 2019 pada 09.33
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kartupintar`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jeniskartu`
--

CREATE TABLE `tb_jeniskartu` (
  `id_jeniskartu` varchar(50) NOT NULL,
  `nama_jeniskartu` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_jeniskartu`
--

INSERT INTO `tb_jeniskartu` (`id_jeniskartu`, `nama_jeniskartu`) VALUES
('JKN-0001', 'Microprocessor Card'),
('JKN-0002', 'Subscriber Identity Module (SIM) Card'),
('JKN-0003', 'Contact Card'),
('JKN-0004', 'Removable User Identity Modul (R-UIM) Card'),
('JKN-0005', 'Contactless Card'),
('JKN-0006', 'Hybrid Card'),
('JKN-0007', 'Universal subscriber Identity Module (USIM)'),
('JKN-0008', 'Memory Card');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kartu`
--

CREATE TABLE `tb_kartu` (
  `id_kartu` varchar(10) NOT NULL,
  `nama_kartu` varchar(50) NOT NULL,
  `storage_kartu` varchar(30) NOT NULL,
  `keterangan_kartu` text NOT NULL,
  `id_jeniskartu` varchar(50) NOT NULL,
  `file_foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kartu`
--

INSERT INTO `tb_kartu` (`id_kartu`, `nama_kartu`, `storage_kartu`, `keterangan_kartu`, `id_jeniskartu`, `file_foto`) VALUES
('KRT-0001', 'RFID Contactless Smartcard EM Proximity 125Khz', '125khz', 'Readonly', 'JKN-0006', 'http://localhost:8080/kartupintar/upload/foto/FOTOKRT-0001.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pesan`
--

CREATE TABLE `tb_pesan` (
  `id_` varchar(255) DEFAULT NULL,
  `fname` varchar(100) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `isi_pesan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pesan`
--

INSERT INTO `tb_pesan` (`id_`, `fname`, `lname`, `email`, `phone`, `isi_pesan`) VALUES
('ID20191004093102', 'Anis ', 'Fajar', 'anisfajar.kudus@gmail.com', '085727109286', 'testing isi pesan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`) VALUES
('2019001', 'Fajar', '8469486d7dbd76bbf47c5fca5de21c87e3795c02cf279a66e37966cb062f471e76894bad0d862781a65bccf0535da3f6995186a540f677d97c7bd92e7557ccf0'),
('2019002', 'Tintin', '7fc1c8045444b11d6fba3e6716cbc4b28de53fca64f7396d287ea03b7ec8c12ed6730b221dc2932a0beab98dccf0c702d65d52f2dbacc460cde188a91ec9f63f');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_jeniskartu`
--
ALTER TABLE `tb_jeniskartu`
  ADD PRIMARY KEY (`id_jeniskartu`);

--
-- Indexes for table `tb_kartu`
--
ALTER TABLE `tb_kartu`
  ADD PRIMARY KEY (`id_kartu`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
